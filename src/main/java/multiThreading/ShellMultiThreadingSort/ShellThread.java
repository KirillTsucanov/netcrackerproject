package multiThreading.ShellMultiThreadingSort;

public class ShellThread extends Thread {

    private int[] part;

    public ShellThread(int[] part) {
        this.part = part;
    }

    @Override
    public void run() {
        shell(part);
    }

    public static void shell(int[] array) {
        int length = array.length;
        int semi = length / 2;
        while (semi > 0) {
            for (int i = 0; i < length - semi; i++) {
                int j = i;
                while ((j >= 0) && array[j] > array[j + semi]) {
                    int temp = array[j];
                    array[j] = array[j + semi];
                    array[j + semi] = temp;
                    j--;
                }
            }
            semi /= 2;
        }
    }
}
