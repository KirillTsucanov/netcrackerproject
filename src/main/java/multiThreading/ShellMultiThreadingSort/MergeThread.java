package multiThreading.ShellMultiThreadingSort;

public class MergeThread extends Thread{
    int[] part1;
    int[] part2;
    int[] res;

    public MergeThread(int[] part1, int[] part2) {
        this.part1 = part1;
        this.part2 = part2;
    }

    @Override
    public void run() {
    res =  merge(part1,part2);
    }

    public static int[] merge(int[] part1, int[] part2) {
        int length = part1.length + part2.length;
        int[] merged = new int[length];
        int i1 = 0;
        int i2 = 0;
        for (int i = 0; i < length; i++) {
            if (i1 == part1.length) {
                merged[i] = part2[i2++];
            } else if (i2 == part2.length) {
                merged[i] = part1[i1++];
            } else {
                if (part1[i1] < part2[i2]) {
                    merged[i] = part1[i1++];
                } else {
                    merged[i] = part2[i2++];
                }
            }
        }
        return merged;
    }

    public int[] getRes() {
        return res;
    }
}
