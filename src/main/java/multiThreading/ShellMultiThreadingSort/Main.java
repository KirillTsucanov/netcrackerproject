package multiThreading.ShellMultiThreadingSort;

import java.util.Random;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        Random random = new Random();
        int[] array = new int[10];
        int[] res;

        int cenr = array.length / 2;
        int[] part1 = new int[cenr];
        int[] part2;
        if (cenr * 2 != array.length) {
            part2 = new int[cenr + 1];
        } else part2 = new int[cenr];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10);
            if (i < cenr) {
                part1[i] = array[i];
            } else {
                part2[i - cenr] = array[i];
            }
        }
        for (int i = 0; i < part1.length; i++) {
            System.out.print(part1[i]);
        }
        for (int i = 0; i < part2.length; i++) {
            System.out.print(part2[i]);
        }
        System.out.println();
        ShellThread sr1 = new ShellThread(part1);
        ShellThread sr2 = new ShellThread(part2);
        sr1.start();
        sr2.start();
        sr2.join();
        MergeThread mt = new MergeThread(part1, part2);
        mt.start();
        mt.join();
        res = mt.getRes();
        for (int i = 0; i < res.length; i++) {
            System.out.print(res[i]);
        }
        System.out.println(mt.hashCode());
    }
}
